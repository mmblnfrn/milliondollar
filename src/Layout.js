import React from 'react';
import { Link } from 'react-router'
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';

export default function Layout({children}) {
  return <div className="Layout container test">
      <div><Link to='/projects'>Projects</Link></div>
      <div><Link to='/members'>Members</Link></div>
      <div><Link to='/tasks'>Tasks</Link></div>
      {children}
    </div>;
}
