import React from 'react';
import './App.css';
import Layout from './Layout';
/*
import ProjectComponent from './Projects';
*/

import { Router, Route, Redirect } from 'react-router';
import createHashHistory from 'history/lib/createHashHistory';


const history = createHashHistory();

function Projects() {
  return (
    <div className="row">
      <div className="col-3"></div>
      <div className="col-6">Projects</div>
  </div>);
}

function Members() {
  return <div className="row">
  <div className="col-4"></div>
  <div className="col-6">Members</div>
</div>;
}

function Tasks() {
  return <div className="row">
  <div className="col-5"></div>
  <div className="col-6">Tasks</div>
</div>;
}

const router = <Router history={history}>
  <Redirect from='/' to='/projects'/>
  <Route path='/' component={Layout}>
    <Route path='projects' component={Projects}  />
    <Route path='members' component={Members}  />
    <Route path='tasks' component={Tasks}  />
  </Route>
</Router>;

export default function App() {
  return <div>{router}</div>;
}
