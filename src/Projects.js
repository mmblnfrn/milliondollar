import React from 'react';
import PropTypes from 'prop-types';
import {Button} from 'react-bootstrap';

// TODO css, 

function ProjectAdd() {
    return <div className="row">
        <div className="col-sm-12 col-md-2 projectAdd">
            <Button bsStyle="primary" className="btn btn-primary btn-block">Neues Projekt</Button>
        </div>
        <div className="col-sm-12 col-md-2 projectAdd">
            <Button className="btn btn-secondary dropdown-toggle button-block dropdown" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Dropdown
            </Button>
            <div className="dropdown-menu" aria-labelledby="dropdownMenu2">
                <Button className="dropdown-item" type="button">Action</Button>
                <Button className="dropdown-item" type="button">Another action</Button>
                <Button className="dropdown-item" type="button">Something else here</Button>
            </div>
        </div>
        <div className="col-sm-12 col-md-2 projectAdd">
            <Button bsStyle="primary" className="btn btn-primary btn-block">Nur meine Projekte</Button>
        </div>
        <div className="col-md-6"/>
    </div>        
}

function Project (props) {
    return <div className="col-12 project" onClick={props.onClickHandler}>
        {props.id}: {props.title}
    </div>
}


export default class ProjectComponent extends React.Component {
    constructor(props) {
        super(props);
    }

    onProjectClick(project) {
        console.log(`Project ${project.title}`);
    }

    render() {
        return <div className="container">
            <ProjectAdd />
            {this.props.projects.projectList.map( p => <div key={p.id} className="row">
                <Project {...p} onClickHandler={ ()=>this.onProjectClick(p) } />
            </div>)}    
        </div>; 
    }

}
